package anon.trollegle;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TimeParser extends CommonParser {

    private static String[] hour = {"h", "hr", "hrs"};
    private static String[] minute = {"m", "min", "mins", "mn"};
    private static String[] second = {"s", "sec", "secs"};
    private static String[] ms = {"ms"};
    private static Pattern unitPattern = Pattern.compile("[a-zA-Z]+");
    
    private long result, current, exponent;
    private Matcher unitMatcher;
    
    public static long parse(String input) {
        return new TimeParser(input).parse();
    }
    
    private TimeParser(String input) {
        super(input);
        unitMatcher = unitPattern.matcher(input);
    }
    
    private long parse() {
        skipSpace();
        do {
            if (!parseNumber())
                throw new IllegalArgumentException("Not a number: " + input.substring(pos));
            skipSpace();
            if (!parseUnit())
                throw new IllegalArgumentException("Not a unit: " + input.substring(pos));
            skipSpace();
        } while (!end());
        return result;
    }
    
    private boolean parseNumber() {
        int start = pos;
        String expr = consumeRegex(numberMatcher);
        if (expr == null)
            return false;
        try {
            if (expr.indexOf('.') == -1) {
                current = Long.parseLong(expr);
                exponent = 0;
            } else {
                current = Long.parseLong(expr.replace(".", ""));
                exponent = expr.length() - 1 - expr.indexOf('.');
            }
            return true;
        } catch (NumberFormatException e) {
            pos = start;
            return false;
        }
    }
    
    private boolean parseUnit() {
        if (end()) {
            result += current;
            return true;
        }
        int start = pos;
        String expr = consumeRegex(unitMatcher);
        if (expr == null)
            return false;
        if (checkUnit(expr, hour, 60 * 60 * 1000)
                || checkUnit(expr, minute, 60 * 1000)
                || checkUnit(expr, second, 1000)
                || checkUnit(expr, ms, 1))
            return true;
        pos = start;
        return false;
    }
    
    private boolean checkUnit(String unit, String[] symbols, long factor) {
        if (Arrays.binarySearch(symbols, unit) >= 0) {
            if (result < 0)
                result -= current * factor / Math.pow(10, exponent);
            else
                result += current * factor / Math.pow(10, exponent);
            return true;
        }
        return false;
    }
}
