package anon.trollegle;

public class SvetlanaUser extends MultiUser {

    public SvetlanaUser(Callback<MultiUser> callback) {
        super(callback);
    }
    
    @Override
    protected void generatePulse() {
        setPulse(Util.chooseWord(), Util.chooseWord());
    }
    
    @Override
    protected String getTopics() {
        if (isPulse()) {
            String[] words = getPulseWords().split(", ");
            return "&topics=" + JsonValue.wrap(new String[] {words[0], words[1], "Svetlana"});
        }
        return super.getTopics();
    }
    
    @Override
    public SvetlanaUser fill(boolean accepted, boolean questionMode, ProxyConfig proxy) {
        super.fill(false, questionMode, proxy);
        return this;
    }

}
