package anon.trollegle;

@Persistent
public class FloodMeter {
    @Persistent
    long lastCall;
    @Persistent
    double bucket;
    
    public boolean allow(long count, long span) {
        long now = System.currentTimeMillis();
        bucket += (double) (now - lastCall) * count / span;
        lastCall = now;
        if (bucket < 1)
            return false;
        if (bucket > count)
            bucket = count;
        bucket -= 1;
        return true;
    }

    private static final JsonSerializer<FloodMeter> serializer = 
        new JsonSerializer<>(FloodMeter.class);
}
